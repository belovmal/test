﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace CurrencyLoader
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1. Загрузка текста странички в приложение

            // Вариант 1 (в потоке главного окна)
            var client = new WebClient();
            string content = client.DownloadString("https://cbr.ru");
            Console.WriteLine(content);

            
            // Вариант 2 (в параллельном потоке)
            Task.Run(async () =>
            {
              var httpClient = new HttpClient();
              string content2 = await httpClient.GetStringAsync("https://cbr.ru");

              Console.WriteLine(content2);
            
            }).GetAwaiter().GetResult();

            
        }
    }
}
